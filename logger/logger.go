package logger

import (
	"encoding/csv"
	"discogs-connector/utils"
)

func WriteLogNotFoundReleases(data []string) {
	file := utils.CreateFileNotFoundReleases()
	defer file.Close()

	writer := csv.NewWriter(file)
	defer writer.Flush()

	err := writer.Write(data)
	utils.CheckError("Cannot write to file", err)
}
