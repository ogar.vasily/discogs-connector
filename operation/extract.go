package operation

import (
	"os"
	"encoding/csv"
	"github.com/dailyburn/ratchet/data"
	"github.com/dailyburn/ratchet/util"
	"strconv"
)

type extract struct {
	filePath string
}

func NewExtract(path string) *extract {
	return &extract{
		filePath: path,
	}
}

type CsvData struct {
	Sku       string `json:"sku,omitempty"`
	ReleaseId int    `json:"release_id,omitempty"`
}

func (e *extract) extract() (error, [][]string) {
	f, err := os.Open(e.filePath)
	if err != nil {
		return err, nil
	}
	defer f.Close()

	rows, err := csv.NewReader(f).ReadAll()
	if err != nil {
		return err, rows
	}

	return nil, rows
}

func (e *extract) ProcessData(d data.JSON, outputChan chan data.JSON, killChan chan error) {
	err, rows := e.extract()
	util.KillPipelineIfErr(err, killChan)

	items := []CsvData{}

	for i, v := range rows {
		if i > 0 {
			if v[0] != "" && v[1] != "" {
				csvData := CsvData{}
				csvData.Sku = v[0]
				releaseId, err := strconv.ParseInt(v[1], 10, 64)
				util.KillPipelineIfErr(err, killChan)
				csvData.ReleaseId = int(releaseId)
				items = append(items, csvData)
			}
		}
	}

	if len(rows) > 0 {
		dd, err := data.NewJSON(items)
		util.KillPipelineIfErr(err, killChan)
		outputChan <- dd
	}
}

func (t *extract) Finish(outputChan chan data.JSON, killChan chan error) {}
