package operation

import (
	"io"
	"github.com/dailyburn/ratchet/util"
	"github.com/dailyburn/ratchet/processors"
)

func NewCustomCSVWriter(w io.Writer) *util.CSVWriter {
	writer := util.NewCSVWriter(w)
	writer.Comma = '|'
	writer.AlwaysEncapsulate = false
	writer.QuoteEscape = `"`
	return writer
}

// NewCSVWriter returns a new CSVWriter wrapping the given io.Writer object
func NewCSVWriter(w io.Writer) *processors.CSVWriter {
	proc := processors.NewCSVWriter(w)
	proc.Writer = NewCustomCSVWriter(w)
	return proc
}
