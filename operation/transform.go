package operation

import (
	log "discogs-connector/logger"
	"github.com/dailyburn/ratchet/data"
	"github.com/dailyburn/ratchet/logger"
	"github.com/dailyburn/ratchet/util"
	discogs "gitlab.com/ogar.vasily/go-discogs"
	response "gitlab.com/ogar.vasily/go-discogs/models"
	"reflect"
	"regexp"
	"strconv"
	"strings"
	"time"
)

type transform struct {
}

func NewTransform(path string) *transform {
	discogs.SetCredentials(path)
	return &transform{}
}

type TransformedData struct {
	Sku           string `json:"sku"`
	ReleaseId     int    `json:"disc_release_id"`
	Name          string `json:"name"`
	MusicArtist   string `json:"music_artist"`
	DiscogsArtist string `json:"discogs_artist"`
	MusicTitle    string `json:"music_title"`
	DiscogsTitle  string `json:"discogs_title"`
	MusicRecordLabel            string `json:"music_record_label"`
	DiscogsRecordLabel     string `json:"discogs_record_label"`
	DiscogsCatalogNo       string `json:"discogs_catalog_no"`
	DiscogsReleaseCountry  string `json:"discogs_release_country"`
	DiscogsReleaseDate     string `json:"discogs_release_date"`
	DiscogsGenre           string `json:"discogs_genre"`
	DiscogsStyle           string `json:"discogs_style"`
	DiscogsTracklist       string `json:"discogs_tracklist"`
	DiscogsEstimatedWeight int    `json:"discogs_estimated_weight"`
	DiscogsNotes           string `json:"discogs_notes"`
	DiscogsReleaseVideo    string `json:"discogs_release_video"`
	//FormatQty              int    `json:"format_qty"`
	DiscogsAllFormatQty int    `json:"discogs_all_format_qty"`
	DiscogsFormat       string `json:"discogs_format"`
	DiscogsMasterId     int `json:"discogs_master_id"`
	GalleryImageURI     string `json:"gallery"`
	BaseImageURI        string `json:"image"`
	SmallImageURI       string `json:"small_image"`
	ThumbnailImageURI   string `json:"thumbnail"`
}

func (t *transform) ProcessData(d data.JSON, outputChan chan data.JSON, killChan chan error) {
	var rowsData []CsvData
	err := data.ParseJSON(d, &rowsData)
	util.KillPipelineIfErr(err, killChan)

	rate := time.Second * 2
	throttle := time.Tick(rate)
	for _, v := range rowsData {
		<-throttle
		release := t.callDiscogs(v.Sku, v.ReleaseId)
		if release.ID != 0 {
			transformedData := t.dataTranformation(v.Sku, release)

			dd, err := data.NewJSON(transformedData)
			util.KillPipelineIfErr(err, killChan)
			outputChan <- dd
		}
	}
}

func (t *transform) Finish(outputChan chan data.JSON, killChan chan error) {}

func (t *transform) callDiscogs(sku string, releaseId int) *response.Release {
	err, release := discogs.GetReleaseById(releaseId)

	if err != nil {
		row := []string{}
		row = append(row, sku, strconv.Itoa(releaseId))
		log.WriteLogNotFoundReleases(row)
		logger.ErrorWithoutTrace("Discogs API error: ", err)
	}

	return release
}

func (t *transform) dataTranformation(sku string, release *response.Release) *TransformedData {
	d := new(TransformedData)

	d.Sku = sku
	d.ReleaseId = release.ID
	d.Name = t.prepareName(release)
	d.DiscogsCatalogNo = t.prepareCatNo(release)
	d.DiscogsArtist = t.prepareDiscogsArtist(release)
	d.MusicArtist = t.prepareArtist(release)
	d.MusicTitle = release.Title
	d.DiscogsTitle = release.Title
	d.MusicRecordLabel = t.prepareLabel(release)
	d.DiscogsRecordLabel = t.prepareDiscogsRecordLabel(release)
	d.DiscogsReleaseCountry = release.Country
	d.DiscogsReleaseDate = release.Released
	d.DiscogsGenre = t.prepareGenre(release)
	d.DiscogsStyle = t.prepareStyle(release)
	d.DiscogsTracklist = t.prepareTracklist(release)
	d.DiscogsEstimatedWeight = release.EstimatedWeight
	d.DiscogsNotes = release.Notes
	d.DiscogsReleaseVideo = t.prepareYoutube(release)
	d.DiscogsAllFormatQty = release.FormatQuantity
	d.DiscogsFormat = t.prepareFormat(release)
	d.DiscogsMasterId = release.MasterID
	d.GalleryImageURI = t.prepareGallery(release)
	d.BaseImageURI = t.prepareImage(release)
	d.SmallImageURI = t.prepareImage(release)
	d.ThumbnailImageURI = t.prepareImage(release)

	return d
}

func (t *transform) prepareName(release *response.Release) string {
	artist := t.prepareArtist(release)

	return artist + " - " + release.Title
}

func (t *transform) getTextSeparator(total, index int) string {
	sep := ", "
	if total-1 == index {
		sep = ""
	}

	return sep
}

func (t *transform) prepareCatNo(release *response.Release) string {
	var catNo string
	tmp := []string{}

	for _, v := range release.Labels {
		if (!t.inArray(v.CatNo, tmp)) {
			tmp = append(tmp, v.CatNo)
		}
	}

	total := len(tmp)
	for i, v := range tmp {
		catNo += v + t.getTextSeparator(total, i)
	}

	return catNo
}

func (t *transform) prepareDiscogsArtist(release *response.Release) string {
	var artist string
	total := len(release.Artists)

	for i, v := range release.Artists {
		artist += v.Name + t.getTextSeparator(total, i)
	}

	return artist
}

func (t *transform) prepareArtist(release *response.Release) string {
	var artist string
	total := len(release.Artists)
	var re = regexp.MustCompile(`\s\(\d+\)`)

	for i, v := range release.Artists {
		name := re.ReplaceAllString(v.Name, "")
		artist += name + t.getTextSeparator(total, i)
	}

	return artist
}

func (t *transform) prepareLabel(release *response.Release) string {
	var label string
	total := len(release.Labels)
	var re = regexp.MustCompile(`\s\(\d+\)`)

	for i, v := range release.Labels {
		name := re.ReplaceAllString(v.Name, "")
		label += name + t.getTextSeparator(total, i)
	}

	return label
}

func (t *transform) prepareDiscogsRecordLabel(release *response.Release) string {
	var label string
	total := len(release.Labels)

	for i, v := range release.Labels {
		label += v.Name + t.getTextSeparator(total, i)
	}

	return label
}

func (t *transform) prepareGenre(release *response.Release) string {
	var genre string
	total := len(release.Genres)

	for i, v := range release.Genres {
		arr := strings.Split(v, ",")
		arrLen := len(arr)
		subtotal := t.getSubtotal(arrLen, total, i)

		if arrLen > 1 {
			for ind, el := range arr {
				genre += strings.Trim(el, " &") + t.getFieldSeparator(subtotal, ind)
			}
		} else {
			genre += strings.TrimSpace(v) + t.getFieldSeparator(total, i)
		}
	}

	return genre
}

func (t *transform) getSubtotal(subtotal, total, outerIndex int) int {
	if subtotal > 1 {
		if outerIndex < (total - 1) {
			subtotal++
		}
	}
	return subtotal
}

func (t *transform) prepareStyle(release *response.Release) string {
	var style string
	total := len(release.Styles)

	for i, v := range release.Styles {
		style += v + t.getFieldSeparator(total, i)
	}

	return style
}

func (t *transform) getFieldSeparator(total, index int) string {
	sep := ","
	if total-1 == index {
		sep = ""
	}

	return sep
}

func (t *transform) getImageFieldSeparator(total, index int) string {
	sep := ";"
	if total-1 == index {
		sep = ""
	}

	return sep
}

func (t *transform) prepareTracklist(release *response.Release) string {
	tracklist := ""
	tableBegin := "<table class='playlist' itemscope='' itemtype='http://schema.org/MusicGroup'><tbody>"
	tableBody := ""
	tableEnd := "</tbody></table>"

	for _, v := range release.Tracklist {
		trBegin := "<tr class='tracklist_track track' itemprop='track' itemscope='' itemtype='http://schema.org/MusicRecording'>"
		position := "<td class='tracklist_track_pos'>" + v.Position + "</td>"
		title := "<td class='track tracklist_track_title'><span class='tracklist_track_title' itemprop='name'>" + v.Title + "</span></td>"
		duration := "<td width='25' class='tracklist_track_duration'><meta itemprop='duration'><span>" + v.Duration + "</span></td>"
		tr := trBegin + position + title + duration + "</tr>"
		tableBody += tr
	}

	if len(release.Tracklist) > 0 {
		tracklist = tableBegin + tableBody + tableEnd
	}

	return tracklist
}

func (t *transform) prepareYoutube(release *response.Release) string {
	var video string
	total := len(release.Videos)

	for i, v := range release.Videos {
		video += v.URI + t.getFieldSeparator(total, i)
	}

	return video
}

func (t *transform) prepareGallery(release *response.Release) string {
	var image string
	total := len(release.Images)

	for i, v := range release.Images {
		if (v.Height > 499) {
			image += v.URI + t.getImageFieldSeparator(total, i)
		}
	}
	return image
}

func (t *transform) prepareImage(release *response.Release) string {
	var image string

	for _, v := range release.Images {
		if (v.Height > 499) {
			image = v.URI
			break
		}
	}
	return image
}

func (t *transform) prepareFormat(release *response.Release) string {
	var format string
	total := len(release.Formats)

	for _, v := range release.Formats {
		subtotal := len(v.Descriptions)
		sep := ""
		if subtotal > 0 {
			sep = ","
		}
		format += v.Name + sep

		if total > 1 {
			subtotal++ // Increased subtotal value to set comma after last format description
		}
		for j, f := range v.Descriptions {
			format += f + t.getFieldSeparator(subtotal, j)
		}
	}

	return format
}

func (t *transform) inArray(needle interface{}, array interface{}) bool {
	val := reflect.Indirect(reflect.ValueOf(array))
	switch val.Kind() {
	case reflect.Slice, reflect.Array:
		for i := 0; i < val.Len(); i++ {
			if needle == val.Index(i).Interface() {
				return true
			}
		}
	}
	return false
}
