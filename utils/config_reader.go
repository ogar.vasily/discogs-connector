package utils

import (
	"path/filepath"
	"errors"
	"os"
	"io/ioutil"
	"encoding/json"
)

func getConfig() map[string]string {
	config := map[string]string{}

	ex, err := os.Executable()
	if err != nil {
		panic(err)
	}
	exPath := filepath.Dir(ex)
	configPath := filepath.Join(exPath, "/configs/config.json")

	b, err := ioutil.ReadFile(configPath)
	if err != nil {
		panic(err)
	}

	err = json.Unmarshal(b, &config)
	if err != nil {
		panic(err)
	}

	return config
}

func GetSentryConfig() string {
	config := getConfig()
	val, ok := config["sentry"]
	if !ok {
		panic(errors.New("Missed config record: sentry"))
	}
	return val
}

func GetDiscogsConfig() string {
	config := getConfig()
	val, ok := config["discogs"]
	if !ok || val == "" {
		panic(errors.New("Missed config record: discogs"))
	}
	return val
}

func GetSmtpIdentityConfig() string {
	config := getConfig()
	val, ok := config["smtp_identity"]
	if !ok {
		panic(errors.New("Missed config record: smtp_identity"))
	}
	return val
}

func GetSmtpUsernameConfig() string {
	config := getConfig()
	val, ok := config["smtp_username"]
	if !ok {
		panic(errors.New("Missed config record: smtp_username"))
	}
	return val
}

func GetSmtpPasswordConfig() string {
	config := getConfig()
	val, ok := config["smtp_password"]
	if !ok {
		panic(errors.New("Missed config record: smtp_password"))
	}
	return val
}

func GetSmtpHostConfig() string {
	config := getConfig()
	val, ok := config["smtp_host"]
	if !ok || val == "" {
		panic(errors.New("Missed config record: smtp_host"))
	}
	return val
}

func GetSmtpServerAddrConfig() string {
	config := getConfig()
	val, ok := config["smtp_server_addr"]
	if !ok || val == "" {
		panic(errors.New("Missed config record: smtp_server_addr"))
	}
	return val
}

func GetMailToConfig() string {
	config := getConfig()
	val, ok := config["mail_to"]
	if !ok || val == "" {
		panic(errors.New("Missed config record: mail_to"))
	}
	return val
}

func GetMailCcConfig() string {
	config := getConfig()
	val, ok := config["mail_cc"]
	if !ok || val == "" {
		panic(errors.New("Missed config record: mail_cc"))
	}
	return val
}

func GetMailFromConfig() string {
	config := getConfig()
	val, ok := config["mail_from"]
	if !ok || val == "" {
		panic(errors.New("Missed config record: mail_from"))
	}
	return val
}
