package utils

import (
	"github.com/jordan-wright/email"
	"net/smtp"
)

func SendMail(to, cc []string, from, subject string, text, html []byte, attachments []string) {
	e := email.NewEmail()
	e.From = from
	e.To = to
	e.Cc = cc
	e.Subject = subject
	e.Text = text
	e.HTML = html

	for _, a := range attachments {
		e.AttachFile(a)
	}

	addr := GetSmtpServerAddrConfig()
	identity := GetSmtpIdentityConfig()
	username := GetSmtpUsernameConfig()
	pass := GetSmtpPasswordConfig()
	host := GetSmtpHostConfig()

	err := e.Send(addr, smtp.PlainAuth(identity, username, pass, host))
	if err != nil {
		CheckError("Send mail errors: ", err)
	}
}
