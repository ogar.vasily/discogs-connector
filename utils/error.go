package utils

import "github.com/getsentry/raven-go"

func CheckError(message string, err error) {
	sentryConfig := GetSentryConfig()
	raven.SetDSN(sentryConfig)

	if err != nil {
		raven.CaptureError(err, nil)
	}
}
