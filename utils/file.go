package utils

import (
	"time"
	"os"
	"path"
	"io"
)

var (
	importFile     string
	exportPath     string
	exportFilePath string
)

func SetImportFilePath(filepath string) {
	importFile = setImportFileName(filepath)
}

func setImportFileName(filepath string) string {
	_, filename := path.Split(filepath)
	return filename
}

func GetImportFileName() string {
	return importFile
}

func SetExportPath(path string) {
	exportPath = path
}

func CreateFilePath(dir, file string) string {
	return path.Join(dir, file)
}

func CreateExportFile() *os.File {
	return createExportFile(importFile, exportPath, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0666)
}

func createExportFile(filename, path string, flag int, perm os.FileMode) *os.File {
	exportFilePath = createExportFilePath(filename, path)

	return createFile(exportFilePath, flag, perm)
}

func GetExportFilePath() string {
	return exportFilePath
}

func createExportFilePath(file, dir string) string {
	exportDir := createExportDirectory(dir)
	fileName := "/export-" + file

	return CreateFilePath(exportDir, fileName)
}

func createExportDirectory(dir string) string {
	t := time.Now().Local()
	formated := t.Format("2006-01-02")
	dirPath := path.Join(dir + "/" + formated)

	return createDirIfNotExist(dirPath)
}

func createFile(filePath string, flag int, perm os.FileMode) *os.File {
	file, err := os.OpenFile(filePath, flag, perm)
	if err != nil {
		CheckError("Cannot create file: ", err)
	}

	return file
}

func createDirIfNotExist(dir string) string {
	cleanDirPath := path.Clean(dir)
	if _, err := os.Stat(cleanDirPath); os.IsNotExist(err) {
		err = os.MkdirAll(cleanDirPath, 0755)
		if err != nil {
			CheckError("Can't create directory: ", err)
		}
	}
	return cleanDirPath
}

func createFileNotFoundReleases() *os.File {
	fileName := "not-found-releases-" + importFile
	exportDir := createExportDirectory(exportPath)
	filePath := CreateFilePath(exportDir, fileName)

	return createFile(filePath, os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0600)
}

func CreateFileNotFoundReleases() *os.File {
	return createFileNotFoundReleases()
}

// Copy the src file to dst. Any existing file will be overwritten and will not
// copy file attributes.
func Copy(src, dst string) error {
	in, err := os.Open(src)
	if err != nil {
		return err
	}
	defer in.Close()

	out, err := os.Create(dst)
	if err != nil {
		return err
	}
	defer out.Close()

	_, err = io.Copy(out, in)
	if err != nil {
		return err
	}
	return out.Close()
}
