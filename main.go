package main

import (
	"flag"
	"fmt"
	"path/filepath"
	"os"
	"discogs-connector/operation"
	"discogs-connector/utils"
	"github.com/dailyburn/ratchet"
	"github.com/dailyburn/ratchet/logger"
)

func main() {
	importFilePath := flag.String("file", "", "write an absolute path to the file")
	exportPath := flag.String("path", "", "write an absolute path to the export folder")
	flag.Parse()

	if *importFilePath == "" {
		fmt.Println("Please set file path as -file=/path/to/the/file")
	} else if *exportPath == "" {
		fmt.Println("Please set export path as -path=/path/to/the/folder")
	} else {
		utils.SetImportFilePath(*importFilePath)
		utils.SetExportPath(*exportPath)

		extract := operation.NewExtract(*importFilePath)

		ex, err := os.Executable()
		if err != nil {
			panic(err)
		}
		exPath := filepath.Dir(ex)

		credentialsPath := filepath.Join(exPath, "/configs/discogs-credentials.json")
		transform := operation.NewTransform(credentialsPath)

		exportFile := utils.CreateExportFile()
		writeCSV := operation.NewCSVWriter(exportFile)

		runPipeline(extract, transform, writeCSV)
		exportFilePath := utils.GetExportFilePath()
		copyFilePath := utils.CreateFilePath(*exportPath, utils.GetImportFileName())
		utils.Copy(exportFilePath, copyFilePath)

		attachments := []string{}
		attachments = append(attachments, *importFilePath, exportFilePath)
		to := []string{utils.GetMailToConfig()}
		cc := []string{utils.GetMailCcConfig()}
		from := utils.GetMailFromConfig()
		subject := "Report of fetched Discogs releases"
		text := []byte("Report about fetched releases from Discogs")
		html := []byte("Report about fetched releases from Discogs")
		utils.SendMail(to, cc, from, subject, text, html, attachments)
	}
}

func runPipeline(extract, transform, writeCSV ratchet.DataProcessor) {
	pipeline := ratchet.NewPipeline(extract, transform, writeCSV)
	pipeline.Name = "My Pipeline"
	//pipeline.PrintData = true

	err := <-pipeline.Run()
	if err != nil {
		utils.CheckError("Pipeline errors: ", err)
		logger.ErrorWithoutTrace(pipeline.Name, ":", err)
		logger.ErrorWithoutTrace(pipeline.Stats())
	} else {
		logger.Info(pipeline.Name, ": Completed successfully.")
	}
}

